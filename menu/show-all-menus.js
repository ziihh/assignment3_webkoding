(function () {
    var helpers = HELPERS();

    var i, j;
    //loop to go through all the categories
    for (i = 0; i < categories.length; i++) {
        //For each category, we create a <h3> with the category name
        //and it is added to the body




        var h3 = document.createElement("h3");
        h3.innerHTML = categories[i];
        document.body.appendChild(h3);
        document.getElementById("menu-container").appendChild(h3);


        //loop to go through all the dishes that belong to category[i] (i.e, the current category)
        var currentDishes = dishes[categories[i]];
        for (j = 0; j < currentDishes.length; j++) {
            //for each menu, a html div with the name, description and price is created and it is added to the body.
            var menuItem = helpers.getHTMLMenuFromTitleDescriptionPrice(currentDishes[j].name, currentDishes[j].description, currentDishes[j].price);

            document.getElementById("menu-container").appendChild(menuItem);

           

        }


    }
})();